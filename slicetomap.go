package remap

//todo: support for embeded struct

import (
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/reflectx"
	"reflect"
)

func indirect(v interface{}) (reflect.Value, error) {

	dstValue := reflect.ValueOf(v)

	if dstValue.Kind() != reflect.Ptr {
		return reflect.Value{}, errors.New("dst must be a pointer")
	}
	if dstValue.IsNil() {
		return reflect.Value{}, errors.New("nil is invalid destination")
	}

	dstDirect := reflect.Indirect(dstValue)

	return dstDirect, nil

}

func KeyName(dstType reflect.Type) (string, error) {

	dstType = reflectx.Deref(dstType)

	if dstType.Kind() == reflect.Slice || dstType.Kind() == reflect.Map {
		//typ prvku pole nebo mapy
		return KeyName(dstType.Elem())

	}

	for i := 0; i < dstType.NumField(); i++ {
		tag := dstType.Field(i).Tag.Get("remap")
		if tag == "key" {
			return dstType.Field(i).Name, nil
		}
	}
	return "", errors.New("tag not found")

}

func getIndex(v reflect.Value, key string) reflect.Value {
	el := reflect.Indirect(v)
	//hodnota ID
	return el.FieldByName(key)
}

type remapdata struct {
	dstValue       reflect.Value
	mapaType       reflect.Type
	dstElementType reflect.Type
	dstPointer     bool
	dstSliceType   reflect.Type
	dstType        reflect.Type
	srcKind        reflect.Kind
	srcValue       reflect.Value
	srcPointer     bool
	srcKey         string
	dstField       string
	sql            bool
	slice          bool
}

func (r *remapdata) newSlice() reflect.Value {
	return reflect.Indirect(reflect.MakeSlice(r.dstSliceType, 0, 10))
}

func (r *remapdata) newDstElement() reflect.Value {
	return reflect.Indirect(reflect.New(r.dstElementType))
}

func (r *remapdata) srcElement(idx int) reflect.Value {
	return r.srcValue.Index(idx)
}

func (r *remapdata) dstElementOrNew(idx reflect.Value) reflect.Value {
	//zjistime jesli pole uz ma tohle id potrebny slice
	atIdx := reflect.Indirect(r.dstValue.MapIndex(idx))

	//pokud na indexu nic neni tak zalozime novy slice
	if !atIdx.IsValid() {
		r.dstValue.SetMapIndex(idx, r.newSlice())
		atIdx = reflect.Indirect(r.dstValue.MapIndex(idx))
	}

	return atIdx
}

func (r *remapdata) addPtr(idx reflect.Value, element reflect.Value) {
	atIdx := r.dstElementOrNew(idx)
	r.dstValue.SetMapIndex(idx, reflect.Append(atIdx, element.Addr()))
}
func (r *remapdata) add(idx reflect.Value, element reflect.Value) {
	atIdx := r.dstElementOrNew(idx)
	r.dstValue.SetMapIndex(idx, reflect.Append(atIdx, element))
}

func (r *remapdata) setPtr(idx reflect.Value, element reflect.Value) {
	r.dstValue.SetMapIndex(idx, element.Addr())
}

func (r *remapdata) set(idx reflect.Value, element reflect.Value) {
	r.dstValue.SetMapIndex(idx, element)
}

func (r *remapdata) getSetFunc() func(idx reflect.Value, element reflect.Value) {
	if r.dstPointer && !r.srcPointer {
		return r.setPtr
	}
	return r.set
}
func (r *remapdata) getAddFunc() func(idx reflect.Value, element reflect.Value) {
	if r.dstPointer && !r.srcPointer {
		return r.addPtr
	}
	return r.add
}

func (r *remapdata) srcID(srcElement reflect.Value) reflect.Value {
	return getIndex(srcElement, r.srcKey)
}

func (r *remapdata) srcLen() int {
	if !r.sql {
		return r.srcValue.Len()
	}
	return 0
}

func newRemap(dst interface{}, data interface{}) (*remapdata, error) {
	d := remapdata{sql: false}
	var err error

	if reflect.TypeOf(data) == reflect.TypeOf(&sqlx.Rows{}) {
		d.sql = true
	}

	d.dstValue, err = indirect(dst)
	if err != nil {
		return nil, err
	}

	//base type of dst
	d.dstType = d.dstValue.Type()
	if d.dstType.Kind() != reflect.Map {
		return nil, fmt.Errorf("expected %s but got %s", reflect.Map, d.dstType.Kind())
	}

	//slice in map type - if pointer, get value type
	d.dstElementType = reflectx.Deref(d.dstType.Elem())
	d.dstPointer = d.dstType.Elem().Kind() == reflect.Ptr
	if d.dstElementType.Kind() == reflect.Slice {
		d.dstSliceType = d.dstElementType
		d.slice = true
		d.dstPointer = d.dstElementType.Elem().Kind() == reflect.Ptr
		d.dstElementType = reflectx.Deref(d.dstElementType.Elem())
	}

	//if input is sql, than we have to check dst for key name
	if d.sql {

		d.srcKey, err = KeyName(d.dstElementType)

	} else {
		d.srcKey, err = KeyName(reflect.TypeOf(data))

		d.srcKind = reflect.TypeOf(data).Kind()

		d.srcValue = reflect.ValueOf(data)

		if d.srcKind != reflect.Slice {
			return nil, errors.New("source must be a slice")
		}

		d.srcPointer = d.srcValue.Type().Elem().Kind() == reflect.Ptr

	}

	if err != nil {
		return nil, err
	}

	return &d, nil
}

//Scan sql rows into map[key]struc. As key is used one of fields in the target struct
//designated by tag 'remap:"key"'. If dst is map[key][]Struct, than duplicate keys ends up in slice
//otherwise duplicate overwrite map elements
func SQLScan(dest interface{}, r *sqlx.Rows) error {

	rm, err := newRemap(dest, r)
	if err != nil {
		return err
	}

	if rm.slice {
		fadd := rm.getAddFunc()
		for r.Next() {

			ndst := rm.newDstElement()
			errScan := r.StructScan(ndst.Addr().Interface())
			if errScan != nil {
				return errScan
			}

			fadd(rm.srcID(ndst.Addr()), ndst)

		}
	} else {
		fset := rm.getSetFunc()

		for r.Next() {

			ndst := rm.newDstElement()
			errScan := r.StructScan(ndst.Addr().Interface())
			if errScan != nil {
				return errScan
			}

			fset(rm.srcID(ndst.Addr()), ndst)

		}
	}

	return nil

}

func ToSlice(dst interface{}, data interface{}) error {

	r, err := newRemap(dst, data)
	if err != nil {
		return err
	}

	if r.slice {
		fadd := r.getAddFunc()
		for i := 0; i < r.srcLen(); i++ {

			elPtr := r.srcElement(i)

			// pridat do mapy
			fadd(r.srcID(elPtr), elPtr)
		}
	} else {
		fset := r.getSetFunc()
		for i := 0; i < r.srcLen(); i++ {

			elPtr := r.srcElement(i)

			// nastavit v mape na indexu
			fset(r.srcID(elPtr), elPtr)
		}

	}

	return nil

}
