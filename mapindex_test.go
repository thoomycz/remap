package remap

import (
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"log"
	"reflect"
	"testing"

	_ "github.com/go-sql-driver/mysql"
)

type ImageRow struct {
	IdImage int64 `remap:"key"`
	Tag     string
	Polozky []ImageRow `remap:"dst"`
}

func dumpJson(i interface{}) {
	s, _ := json.MarshalIndent(i, "", "  ")
	log.Println(string(s))
}

func makeData() interface{} {
	retTags := make([]ImageRow, 0)
	retTags = append(retTags, ImageRow{IdImage: 1, Tag: "A"})
	retTags = append(retTags, ImageRow{IdImage: 1, Tag: "A1"})
	retTags = append(retTags, ImageRow{IdImage: 11, Tag: "B"})
	retTags = append(retTags, ImageRow{IdImage: 12, Tag: "C"})
	retTags = append(retTags, ImageRow{IdImage: 15, Tag: "D"})

	return retTags
}

func makeDataPtr() interface{} {
	retTags := make([]*ImageRow, 0)
	retTags = append(retTags, &ImageRow{IdImage: 1, Tag: "A"})
	retTags = append(retTags, &ImageRow{IdImage: 1, Tag: "A1"})
	retTags = append(retTags, &ImageRow{IdImage: 11, Tag: "B"})
	retTags = append(retTags, &ImageRow{IdImage: 12, Tag: "C"})
	retTags = append(retTags, &ImageRow{IdImage: 15, Tag: "D"})

	return retTags
}

func makeRows(t *testing.T) *sqlx.Rows {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()

	rowsdata := sqlmock.NewRows([]string{"idimage", "tag"}).
		AddRow(1, "tag1").
		AddRow(10, "tag10").
		AddRow(12, "test").
		AddRow(12, "test")
	mock.ExpectQuery("^SELECT (.+) FROM tags").WillReturnRows(rowsdata)

	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	rows, err2 := sqlxDB.Queryx("SELECT * FROM tags")
	if err2 != nil {
		t.Error(err2)
	}

	return rows

}

func TestKeyName(t *testing.T) {
	data := makeData()

	kn, err := KeyName(reflect.TypeOf(data))
	if err != nil {
		t.Error(err)
	}

	if kn != "IdImage" {
		t.Error("wrong filed")
	}

}

func TestToSlice(t *testing.T) {

	src := makeData()

	tags := make(map[int64][]ImageRow)

	ToSlice(&tags, src)

	if tags[1][1].Tag != "A1" ||
		tags[15][0].Tag != "D" {
		str, _ := json.MarshalIndent(tags, "", "   ")
		t.Error(string(str))
	}

}

func TestToMap(t *testing.T) {

	src := makeData()

	tags := make(map[int64]ImageRow)

	ToSlice(&tags, src)

	if tags[1].Tag != "A1" ||
		tags[15].Tag != "D" {
		str, _ := json.MarshalIndent(tags, "", "   ")
		t.Error(string(str))
	}

}

func TestToSlicePtr(t *testing.T) {

	src := makeDataPtr()

	tags := make(map[int64][]*ImageRow)

	ToSlice(&tags, src)

	if tags[1][1].Tag != "A1" ||
		tags[15][0].Tag != "D" {
		str, _ := json.MarshalIndent(tags, "", "   ")
		t.Error(string(str))
	}

}

func TestToMapPtr(t *testing.T) {

	src := makeDataPtr()

	tags := make(map[int64]*ImageRow)

	ToSlice(&tags, src)

	if tags[1].Tag != "A1" ||
		tags[15].Tag != "D" {
		str, _ := json.MarshalIndent(tags, "", "   ")
		t.Error(string(str))
	}

}

func TestSQLScanStruct(t *testing.T) {

	rows := makeRows(t)

	dst := make(map[int64]ImageRow)

	err3 := SQLScan(&dst, rows)
	if err3 != nil {
		t.Error(err3)
	}

	if dst[1].Tag != "tag1" {
		t.Error("unexpected value")
	}
	if dst[10].Tag != "tag10" {
		t.Error("unexpected value")
	}

	if len(dst) != 3 {
		t.Error("unexpected length")
	}

}

func TestSQLScanStructPtr(t *testing.T) {

	rows := makeRows(t)

	dst := make(map[int64]*ImageRow)

	err3 := SQLScan(&dst, rows)
	if err3 != nil {
		t.Error(err3)
	}

	if dst[1].Tag != "tag1" {
		t.Error("unexpected value")
	}
	if dst[10].Tag != "tag10" {
		t.Error("unexpected value")
	}

	if len(dst) != 3 {
		t.Error("unexpected length")
	}

}

func TestSQLScanSlice(t *testing.T) {

	rows := makeRows(t)

	dst := make(map[int64][]ImageRow)

	err3 := SQLScan(&dst, rows)
	if err3 != nil {
		t.Error(err3)
	}

	if dst[1][0].Tag != "tag1" {
		t.Error("unexpected value")
	}
	if dst[10][0].Tag != "tag10" {
		t.Error("unexpected value")
	}

	if len(dst) != 3 {
		t.Error("unexpected length")
	}

}

func TestSQLScanSlicePtr(t *testing.T) {

	rows := makeRows(t)

	dst := make(map[int64][]*ImageRow)

	err3 := SQLScan(&dst, rows)
	if err3 != nil {
		t.Error(err3)
	}

	if dst[1][0].Tag != "tag1" {
		t.Error("unexpected value")
	}
	if dst[10][0].Tag != "tag10" {
		t.Error("unexpected value")
	}

	if len(dst) != 3 {
		t.Error("unexpected length")
	}

}
