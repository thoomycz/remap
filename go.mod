module bitbucket.org/thoomycz/remap

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/go-sql-driver/mysql v1.4.0
	github.com/jmoiron/sqlx v1.2.0
)
